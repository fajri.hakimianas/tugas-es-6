const createTable = document.getElementById('create')
createTable.addEventListener('click', () => {
    const data = [
        {    nama: 'Edi Hartono',    umur: 23,    alamat: 'Karangsono'}, 
        {    nama: 'M. Rizal Aldosari',    umur: 24,    alamat: 'Kuripan'}, 
        {    nama: 'Wahyu Setiawan',    umur: 25,    alamat: 'Bumi Rejo'}, 
        {    nama: 'Mahesa Anggraestian',    umur: 20,    alamat: 'Tawangsari'}, 
        {    nama: 'Edow Bagus Pangestu',    umur: 28,    alamat: 'Tlogosari'}
    ];
    
    const nama = data.map((result, i) => i+1+'. ' + '<td>'+ result.nama + '</td>' + '<br/>')
    let n = nama.reduce((accumulator, currentValue) => `${accumulator} ${currentValue}`)

    const umur = data.map((result, i) => '<td>'+ result.umur + '</td>' + '<br/>')
    let u = umur.reduce((accumulator, currentValue) => `${accumulator} ${currentValue}`)

    const alamat = data.map((result, i) => '<td>'+ result.alamat + '</td>' + '<br/>')
    let a = alamat.reduce((accumulator, currentValue) => `${accumulator} ${currentValue}`)

    document.getElementById("td").innerHTML = n
    document.getElementById("age").innerHTML = u
    document.getElementById("address").innerHTML = a

    function coba(params, ...hasil){
        return params.reduce((e,string,r)=>`${e} ${string}<span> ${hasil[r] || ""} </span>`, '')
    }

    function literals(){
        var x = document.getElementById("myDIV");
        if (x.style.display === "none") {
          x.style.display = "block";
        } else {
          x.style.display = "none";
        }
    }
    literals()    
})