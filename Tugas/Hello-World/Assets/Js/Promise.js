const promise = document.getElementById('Promise');
promise.addEventListener('click', () => {
    let data = new Promise((resolve, reject) => {
        let sum = 5 + 10
    
        if(sum === 15) {
            resolve("True")
        }
        else {
            reject('False')
        }
    })
    
    data.then((message) => {
        document.writeln('This ' + message)
    }).catch((message) => {
        document.writeln('This ' + message)
    })
})