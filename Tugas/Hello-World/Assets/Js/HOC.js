
const hoc = document.getElementById('HOC');
hoc.addEventListener('click', () => {
    const data = [    
        {
            nama: 'Edi Hartono',        
            umur: 23,
            alamat: 'Karangsono'
        },
        {
            nama: 'M. Rizal Aldosari',
            umur: 24,
            alamat: 'Kuripan',
        },
        {
            nama: 'Wahyu Setiawan',
            umur: 25,
            alamat: 'Bumi Rejo'
        },
        {
            nama: 'Mahesa Anggraestian',
            umur: 20,
            alamat: 'Tawangsari'
        },
        {
            nama: 'Edow Bagus Pangestu',
            umur: 28,
            alamat: 'Tlogosari'
        }
    ];
    
    const totalUmur = data.filter(res => res.umur % 4 == 0).map(res => res.umur).reduce((acc, cur) => acc + cur)
    document.writeln(totalUmur)
})