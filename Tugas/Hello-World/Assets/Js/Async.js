const asynC = document.getElementById('Async');
asynC.addEventListener('click', () => {

    const postData = [
        {
            title: "Post one",
            body: "This is post one"
        },
        {
            title: "Post two",
            body: "This is post two"
        }
    ]
    
    const createData = (resData) => {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                postData.push(resData)
                const error = false
                if(!error) {
                    resolve()
                }else{
                    reject()
                }
            }, 2000)
        })
    }
    
    const getPosts = () => {
        setTimeout(() => {
            postData.forEach(post => {
                console.log(post)
            })
        }, 1000)
    }
      
    const newPost = {
        title: "Post three",
        body: "This is post three"
    }
      
    const init = async() => {
        await createData(newPost)
        getPosts()
    }
      
   init()
})
