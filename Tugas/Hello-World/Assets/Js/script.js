// const btnClick = document.getElementById('click-me');
// btnClick.addEventListener('click', () => {
//     alert('Dia Menyentuhku..!');
// });

class People {
    // Properti
    constructor(name, umur, energi){
        this.name = name;
        this.umur= umur;
        this.energi= energi;
        this.status= "diam";
    }

    // Method
    makan(porsi){
        this.energi += porsi;
        console.log(`${this.name} sedang makan nasi ayam`)
    }

    berjalan(){
        this.status = "berjalan";
        console.log(`${this.name} sedang berjalan`)
    }

    diam(){
        this.status = "diam";
        console.log(`${this.name} sedang berdiam diri`)
    }

    static isStatus(fajri){
        if(fajri.status === "berjalan"){
            console.log("Fajri sedang Berjalan di Kampus");
        }
        else {
            console.log("Fajri berdiam diri di kamar")
        }
    }

    lahir(){
        let tahun = 2021 - this.umur;
        return tahun;
    }

    tidur(jam){
        this.energi += jam * 2;
        console.log(`Selamat tidur`)
    }

    ngoding(jam){
        this.energi -= jam;
        console.log(`jangan ngoding terlalu lama!`)
    }
}

let fajri = new People('Fajri Hakimi Anas', 25, 10);

People.isStatus(fajri) //diam;
fajri.berjalan();
People.isStatus(fajri) //Berjalan

console.log('Fajri lahir pada tahun', fajri.lahir());
console.log(fajri);